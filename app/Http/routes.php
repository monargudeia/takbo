<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('home');
//});

Route::get('/', 'RaceController@index');

Route::post('race/save', 'RaceController@save');

Route::post('race/test', 'RaceController@test');

Route::get('race/test', function(){
	return view('errors.notfound');
});

Route::get('race/save', function(){
	return view('errors.notfound');
});

