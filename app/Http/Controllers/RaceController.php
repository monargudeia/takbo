<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use App\Race;

use Request;

use App\Http\Requests;



class RaceController extends Controller
{
    
    public function index() {
    	$races = Race::all();
    	return view('home', ['races' => $races]);
    } 

    public function test(){
    	if(Request::isMethod('post')) {
    		if(Request::input('answer') == 'evagreen') {
    			return response()->json(['success' => 1]);
    		} else {
    			return response()->json(['success' => 0]);
    		}
    	}
    }


	public function save() {

		if(Request::isMethod('post')) {
			
			
			$race = new Race;

			$race->event = Request::input('event');
			$race->category = Request::input('category');
			$race->hh = Request::input('hh');
			$race->mm = Request::input('mm');
			$race->ss = Request::input('ss');

			if($race->save()) {
				return response()->json(['success' => 1, 'msg' => 'successfully saved']);
			} 
			
		}
	}

}
