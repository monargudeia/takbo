<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $fillable = ['event', 'category', 'hh', 'mm', 'ss'];
}
