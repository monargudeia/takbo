<!doctype html>

<html>
	<head>
		<meta name="_token" content="{!! csrf_token() !!}"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
		
		<style>
			body {
				margin: 0;
				padding: 0;
				/*background-color: #e5e5e5; */
				font-family: 'Lato', sans-serif;
			}

		</style>
	</head>

	<body>
		
		<div class="container">
		  <div class="jumbotron" style="margin-top:50px">
		    <h1>Hello Monching :)</h1> 
		    <p>Here are your race records</p> 
		  </div>
		
		  <button type="button" data-toggle="modal" data-target="#detailsModal" class="btn btn-primary">Add Race</button>


		  <div id="raceTable" style="margin-top:10px">
		  	<table class="table table-striped">
		  		<thead>
		  			<tr>
		  				<th>Event</th>
		  				<th>Category</th>
		  				<th>Time</th>
		  			</tr>
		  		</thead>
		  		<tbody>
		  			
		  		@foreach($races as $race)
		  			<tr>
		  				<td>{{ $race->event }}</td>	
		  				<td>{{ $race->category }}</td>
		  				<td>{{ $race->hh.':'.$race->mm.':'.$race->ss }}</td>
		  			</tr>


		  			
		  		@endforeach




		  		</tbody>
		  	</table>
		  </div>



		  <!-- Modal -->
		  <div class="modal fade" id="detailsModal" role="dialog">
		    <div class="modal-dialog modal-sm">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h2 class="modal-title">Race Details</h2>
		        </div>

		        <div class="modal-body">
				{!! Form::open(array('url'=>'race/save','method'=>'POST', 'id'=>'raceForm')) !!}
					
					Event: {!! Form::text('event','',array('class' => 'form-control', 'placeholder' => 'Event name...')) !!} <br />
					Category: {!! Form::select('category', array('3k' => '3k', '5k' => '5k', '10k' => '10k', '21k' => '21k', '42k' => '42k'), '3k', ['class' => 'form-control']) !!} <br />
					Time: 
					<div class="input-group">
						{!! Form::text('hh', '00', array('class' => 'form-control', 'maxlength' => 2)) !!}
						<span class="input-group-addon">:</span>
						{!! Form::text('mm', '00', array('class' => 'form-control', 'maxlength' => 2)) !!}
						<span class="input-group-addon">:</span>
						{!! Form::text('ss', '00', array('class' => 'form-control', 'maxlength' => 2)) !!}					
					</div>
		        </div>

		        <div class="modal-footer">
		        	{!! Form::button('Cancel', array('class'=>'btn btn-default','data-dismiss' => 'modal')) !!}
		        	{!! Form::button('Save', array('class'=>'btn btn-primary','id' => 'save')) !!}
		        </div>

				{!! Form::close() !!}
		      </div>
		    </div>
		  </div>
		</div>

			
	<script>
			$(document).ready(function(){

			var event = $('input[name=event]');

			$('#save').click(function(){

				//alert('Oooops joke lang');

				if($.trim(event.val()) == '') {	
					alert('Event name is empty');
				} else {

					var data = $('#raceForm').serialize();
					var tester = prompt('Before you post, What do i really really like?');

					$.ajaxSetup({
  						headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
					});

					$.ajax({
						url: 'race/test',
						type: 'post',
						data: 'answer=' + tester,
						dataType: 'json',
						success: function(data){
							if(data.success == 1) {
								
								$.ajax({
									url:'race/save',
									type:'post',
									data: data,
									dataType:'json',
									beforeSend: function(){
										$('#save').text('Saving...');
									},
									success: function(data){
										if(data.success == 1) {
											$('#detailsModal').hide();
											location.reload();
										} else {
											alert('Something\'s wrong');
										}
									}

								});

							} else {
								alert('Please just go home and plant camote :p');
							}
						}

					});



			
					
				}
			});

		});		
	</script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>		

	</body>
</html>